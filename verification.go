package main

import (
	"gitlab.com/cosban/normalize/internal"
)

func inferTypes(cells []string, columns map[int]string, t []internal.DataType) map[int]internal.DataType {
	types := map[int]internal.DataType{}
	for i, cell := range cells {
		if _, ok := columns[i]; ok {
			for _, v := range t {
				if v.Verify(cell) {
					types[i] = v
					break
				}
			}
		}
	}
	return types
}
