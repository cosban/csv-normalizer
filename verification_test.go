package main

import (
	"testing"

	"github.com/cosban/assert"
	"gitlab.com/cosban/normalize/internal"
	"gitlab.com/cosban/normalize/types"
)

func Test_inferTypes(t *testing.T) {
	assert := assert.New(t)
	given := []string{"TRUE", "false", "this is A string", "4090", "2017-03-19", "another, string", "", "$100.00", "1.8099"}
	givenT := []internal.DataType{
		&types.BooleanType{},
		&types.DecimalType{},
		&types.DateType{},
		&types.NullType{},
		&types.IntegerType{},
		&types.StringType{},
	}
	givenColumns := map[int]string{0: "", 1: "", 2: "", 3: "", 4: "", 5: "", 6: "", 7: "", 8: ""}

	expected := map[int]internal.DataType{
		0: givenT[0],
		1: givenT[0],
		2: givenT[5],
		3: givenT[4],
		4: givenT[2],
		5: givenT[5],
		6: givenT[3],
		7: givenT[1],
		8: givenT[1],
	}
	actual := inferTypes(given, givenColumns, givenT)
	assert.Equals(expected, actual)
}
