package types

import (
	"fmt"
	"strings"
)

type StringType struct{}

func (t *StringType) Verify(value string) bool {
	return true
}

func (t *StringType) Translate(value string) string {
	if n.Verify(value) {
		return n.Translate(value)
	}
	value = strings.Trim(value, " \"'")
	value = strings.Replace(value, "'", "''", -1)
	return fmt.Sprintf("'%s'", value)
}

func (t *StringType) Type() string {
	return "text"
}
