package types

import (
	"regexp"
	"strings"
)

var format = regexp.MustCompile("^\\$?\\d+\\.\\d+$")

type DecimalType struct{}

func (t *DecimalType) Verify(value string) bool {
	return format.Match([]byte(value)) || n.Verify(value)
}
func (t *DecimalType) Translate(value string) string {
	if n.Verify(value) {
		return n.Translate(value)
	}
	value = strings.TrimLeft(value, "$")
	return value
}

func (t *DecimalType) Type() string {
	return "decimal"
}
