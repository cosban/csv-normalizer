package types

import (
	"testing"

	"github.com/cosban/assert"
)

var (
	stringType = &StringType{}
)

func Test_string_normalize(t *testing.T) {
	assert := assert.New(t)

	givenValue := `"'BLAH BLAH BLAH, Dr. Gillis'.'"`
	expected := `'BLAH BLAH BLAH, Dr. Gillis''.'`

	actual := stringType.Translate(givenValue)

	assert.Equals(expected, actual)
}

func Test_string_null_normalize(t *testing.T) {
	assert := assert.New(t)

	givenValue := ``
	expected := `NULL`

	actual := stringType.Translate(givenValue)

	assert.Equals(expected, actual)
}
