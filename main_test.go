package main

import (
	"testing"

	"github.com/cosban/assert"
	"gitlab.com/cosban/normalize/internal"
	"gitlab.com/cosban/normalize/types"
)

func Test_getUsableColumns(t *testing.T) {
	assert := assert.New(t)

	given := []string{"CatID", "IntakeDate", "CatName", "Sex", "CatType", "CoatLength"}
	ignoring := internal.StringSetOf("IntakeDate", "CatName", "CatType", "CoatLength")
	expected := map[int]string{
		0: "CatID",
		3: "Sex",
	}

	actual := getUsableColumns(given, ignoring)
	assert.Equals(expected, actual)
}

func Test_normalize(t *testing.T) {
	assert := assert.New(t)
	given := [][]string{
		{"TRUE", "false", "this is A string", "4090", "2017-03-19", "another, string", "", "$100.00", "1.8099"},
		{"FALSE", "true", "this is A string", "4090", "2017", "another, string", "", "H", "False"},
	}
	givenT := []internal.DataType{
		&types.BooleanType{},
		&types.DecimalType{},
		&types.DateType{},
		&types.NullType{},
		&types.IntegerType{},
		&types.StringType{},
	}
	givenTypes := map[int]internal.DataType{
		0: givenT[0],
		1: givenT[0],
		2: givenT[5],
		3: givenT[4],
		4: givenT[2],
		5: givenT[5],
		6: givenT[3],
		7: givenT[1],
		8: givenT[1],
	}
	expectedValid := []string{"1::bit", "0::bit", "'this is A string'", "4090", "'2017-03-19'", "'another, string'", "NULL", "$100.00", "1.8099"}
	expectedInvalid := []string{"FALSE", "true", "this is A string", "4090", "2017", "another, string", "", "H", "False"}
	actualValid, actualInvalid := normalize(given, givenTypes)
	assert.Equals([][]string{expectedValid}, actualValid)
	assert.Equals([][]string{expectedInvalid}, actualInvalid)
}

func Test_getname(t *testing.T) {
	assert := assert.New(t)

	given := "bob.fat.lard.tina.csv"
	expected := "bob.fat.lard.tina"

	actual := getName(given)

	assert.Equals(expected, actual)
}
