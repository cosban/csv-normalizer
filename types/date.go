package types

import (
	"fmt"
	"time"
)

type DateType struct{}

func (t *DateType) Verify(value string) bool {
	_, err := time.Parse("2006-01-02", value)
	return err == nil || n.Verify(value)
}

func (t *DateType) Translate(value string) string {
	if n.Verify(value) {
		return n.Translate(value)
	}
	return fmt.Sprintf("'%s'", value)
}

func (t *DateType) Type() string {
	return "date"
}
