package main

import (
	"encoding/csv"
	"fmt"
	"os"
	"sort"
	"strings"

	"gitlab.com/cosban/normalize/internal"
)

func writeCSV(records [][]string, name string) {
	f, err := os.Create(name)
	if err != nil {
		panic(err)
	}
	w := csv.NewWriter(f)
	w.WriteAll(records)
}

func writeSQL(name string, records [][]string, columns map[int]string, types map[int]internal.DataType) {
	sqlName := fmt.Sprintf("translated_%s.sql", name)
	f, err := os.Create(sqlName)
	if err != nil {
		panic(err)
	}
	writeTableDef(f, name, columns, types)
	writeHeader(f, name, columns)
	writeRecords(f, records)
}

func writeTableDef(f *os.File, name string, columns map[int]string, types map[int]internal.DataType) {
	indexes := orderIndexes(columns)
	cols := []string{}
	for _, i := range indexes {
		t := types[i]
		col := columns[i]
		cols = append(cols, fmt.Sprintf("%s %s", col, t.Type()))
	}
	create := fmt.Sprintf("CREATE TABLE IF NOT EXISTS legacy_%s (\n\t%s\n);\n", name, strings.Join(cols, ",\n\t"))
	f.WriteString(create)
}

func writeHeader(f *os.File, name string, columns map[int]string) {
	table := fmt.Sprintf("legacy_%s", name)
	ordered := orderColumns(columns)
	insert := fmt.Sprintf("INSERT INTO %s (%s)\nVALUES\n", table, strings.Join(ordered, ","))
	f.WriteString(insert)
}

func orderIndexes(columns map[int]string) []int {
	indexes := []int{}
	for k, _ := range columns {
		indexes = append(indexes, k)
	}
	sort.Ints(indexes)
	return indexes
}

func orderColumns(columns map[int]string) []string {
	indexes := orderIndexes(columns)
	ordered := []string{}
	for _, i := range indexes {
		ordered = append(ordered, columns[i])
	}
	return ordered
}

func writeRecords(f *os.File, records [][]string) {
	prefix := ""
	for _, line := range records {
		row := strings.Join(line, ",")
		f.WriteString(fmt.Sprintf("%s(%s)", prefix, row))
		prefix = ",\n"
	}
	f.WriteString(";")
}
