package internal

var (
	HelpText = `USAGE: normalize [OPTION]... FILE
Normalize csv files for database imports
		
Miscellaneous:
  -h, --help		display this help text and exit
  -v, --version		display version information and exit
`
	VersionText = `normalize 1.0
Copyright (C) 2017 cosban
License MIT: <https://opensource.org/licenses/MIT>
This is free software: you are free to change and redistribute it
There is NO WARRANTY, to the extent permited by law.append

Written by cosban`
)
