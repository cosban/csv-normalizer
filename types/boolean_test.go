package types

import (
	"testing"

	"github.com/cosban/assert"
)

var (
	booleanType = &BooleanType{}
)

func Test_bool_verify_happy(t *testing.T) {
	assert := assert.New(t)

	givenValue := "true"

	assert.True(booleanType.Verify(givenValue))
}

func Test_bool_verify_sad(t *testing.T) {
	assert := assert.New(t)

	givenValue := "mr mittens"

	assert.False(booleanType.Verify(givenValue))
}

func Test_bool_one_sad(t *testing.T) {
	assert := assert.New(t)

	givenValue := "1"

	assert.False(booleanType.Verify(givenValue))
}
