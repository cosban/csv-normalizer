package types

import "strconv"

type IntegerType struct{}

func (t *IntegerType) Verify(value string) bool {
	_, err := strconv.ParseInt(value, 10, 64)
	return err == nil || n.Verify(value)
}
func (t *IntegerType) Translate(value string) string {
	if n.Verify(value) {
		return n.Translate(value)
	}
	return value
}

func (t *IntegerType) Type() string {
	return "integer"
}
