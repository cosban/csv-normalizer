package types

import (
	"testing"

	"github.com/cosban/assert"
)

var (
	dateType = &DateType{}
)

func Test_date_verify_happy(t *testing.T) {
	assert := assert.New(t)

	givenValue := "2017-02-04"

	assert.True(dateType.Verify(givenValue))
}

func Test_date_verify_string_sad(t *testing.T) {
	assert := assert.New(t)

	givenValue := "mr mittens"

	assert.False(dateType.Verify(givenValue))
}

func Test_date_verify_int_sad(t *testing.T) {
	assert := assert.New(t)

	givenValue := "20170204"

	assert.False(dateType.Verify(givenValue))
}

func Test_date_normalize(t *testing.T) {
	assert := assert.New(t)

	givenValue := "1992-09-19"
	expected := `'1992-09-19'`

	actual := dateType.Translate(givenValue)

	assert.Equals(expected, actual)
}

func Test_date_null_normalize(t *testing.T) {
	assert := assert.New(t)

	givenValue := ""
	expected := "NULL"

	actual := dateType.Translate(givenValue)

	assert.Equals(expected, actual)
}
