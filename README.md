# csv-normalizer
> a simple tool for importing csv data into a postgres database

Created for the sake of learning (and memes)

## About

This tool is extremely rudimentary and may not even be useful to you. It is expected that all of the data contained 
within the csv file is to be treated as legacy data which will later be scrubbed so that it can be imported to a 
production quality database. Files must be CSV. The first line in the file must contain all of the column names.
The second line must contain an example of what a perfect row of data would look like. If null cells are found, the
rest of the column is expected to be null.

A form of data validation is performed, where the program checks to see if the all of the data within subsequent rows 
match what was found within the first row. If failures occur, they will be printed within an "invalid" csv. The invalid
csv will contain only rows and cells which are invalid. This is useful for manually fixing the inconsistent data within
the original csv file.

Once the file is validated, cells are translated into an equivalent value that may be used with postgres. Empty cells
are filled with null, booleans are turned into bits, Strings are trimmed and wrapped with single quotes, etc.

This tool will also generate a create table statement with what it believes the schema should look like.

## Ignoring columns

For one reason or another, you may want to ignore the contents of a column. Just add the names of the columns you wish
to ignore after the name of the csv file.

## Usage

normalize [FILENAME] [IGNORED COLUMNS]...
