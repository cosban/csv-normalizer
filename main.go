package main

import (
	"encoding/csv"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"

	"gitlab.com/cosban/normalize/internal"
	"gitlab.com/cosban/normalize/types"
)

var (
	args = os.Args[1:]
	t    = []internal.DataType{
		&types.BooleanType{},
		&types.DecimalType{},
		&types.DateType{},
		&types.NullType{},
		&types.IntegerType{},
		&types.StringType{},
	}
)

func main() {
	err := validateArgs(args)
	if err != nil || args[0] == "-h" || args[0] == "--help" {
		help()
	} else if args[0] == "-v" || args[0] == "--version" {
		version()
	} else {
		fileName := getName(args[0])
		r := getFile(args[0])
		process(fileName, r, args[1:]...)
	}
}

func getName(f string) string {
	segs := strings.Split(f, ".")
	return strings.Join(segs[:len(segs)-1], "")
}

func process(fileName string, r io.Reader, ignored ...string) {
	reader := csv.NewReader(r)
	records, err := reader.ReadAll()
	if err != nil {
		fmt.Printf("Problem reading file: %s\n", err.Error())
		help()
	}
	ignoring := internal.StringSetOf(ignored...)
	columns := getUsableColumns(records[0], ignoring)
	first := readLine(records[1], columns)
	types := inferTypes(first, columns, t)
	valid, invalid := normalize(records[1:], types)
	if len(invalid) > 0 {
		fmt.Printf("There are invalid rows. Please look at invalid_%s for reference", args[0])
		writeCSV(invalid, fmt.Sprintf("invalid_%s", fileName))
	} else {
		fmt.Println("All rows were valid!")
	}
	writeSQL(fileName, valid[1:], columns, types)
}

func getUsableColumns(line []string, ignoring *internal.StringSet) map[int]string {
	columns := map[int]string{}
	for i, name := range line {
		if !ignoring.Contains(name) {
			columns[i] = name
		}
	}
	return columns
}

func readLine(line []string, columns map[int]string) []string {
	cells := []string{}
	for col, cell := range line {
		if _, ok := columns[col]; ok {
			cells = append(cells, cell)
		}
	}
	return cells
}

func normalize(lines [][]string, types map[int]internal.DataType) ([][]string, [][]string) {
	vRecords, iRecords := [][]string{}, [][]string{}
	for line, cells := range lines {
		valid, invalid := []string{}, []string{}
		for i, cell := range cells {
			if _, ok := types[i]; ok {
				if !types[i].Verify(cell) {
					invalid = append(invalid, cell)
				} else {
					cell = types[i].Translate(cell)
					valid = append(valid, cell)
				}
			}
		}
		if len(invalid) > 0 {
			invalid = append([]string{strconv.Itoa(line)}, invalid...)
			iRecords = append(iRecords, invalid)
		} else {
			vRecords = append(vRecords, valid)
		}
	}
	return vRecords, iRecords
}
