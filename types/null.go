package types

type NullType struct{}

var n = &NullType{}

func (t *NullType) Verify(value string) bool {
	return len(value) == 0
}

func (t *NullType) Translate(value string) string {
	return "NULL"
}

func (t *NullType) Type() string {
	panic("Nothing should have a null type")
}
