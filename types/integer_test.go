package types

import (
	"testing"

	"github.com/cosban/assert"
)

var (
	intType = &IntegerType{}
)

func Test_integer_verify_happy(t *testing.T) {
	assert := assert.New(t)

	givenValue := "132"

	assert.True(intType.Verify(givenValue))
}

func Test_integer_verify_string_sad(t *testing.T) {
	assert := assert.New(t)

	givenValue := "NULL"

	assert.False(intType.Verify(givenValue))
}

func Test_integer_verify_decimal_sad(t *testing.T) {
	assert := assert.New(t)

	givenValue := "15.231"

	assert.False(intType.Verify(givenValue))
}

func Test_integer_verify_date_sad(t *testing.T) {
	assert := assert.New(t)

	givenValue := "2015-12-12"

	assert.False(intType.Verify(givenValue))
}


func Test_integer_normalize(t *testing.T) {
	assert := assert.New(t)

	givenValue := "14"
	expected := "14"

	actual := intType.Translate(givenValue)

	assert.Equals(expected, actual)
}

func Test_integer_null_normalize(t *testing.T) {
	assert := assert.New(t)

	givenValue := ""
	expected := "NULL"

	actual := intType.Translate(givenValue)

	assert.Equals(expected, actual)
}
