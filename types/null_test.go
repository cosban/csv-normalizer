package types

import (
	"testing"

	"github.com/cosban/assert"
)

var (
	nullType = &NullType{}
)

func Test_verify_happy(t *testing.T) {
	assert := assert.New(t)

	givenValue := ""

	assert.True(nullType.Verify(givenValue))
}

func Test_verify_sad(t *testing.T) {
	assert := assert.New(t)

	givenValue := "NULL"

	assert.False(nullType.Verify(givenValue))
}

func Test_null_normalize(t *testing.T) {
	assert := assert.New(t)

	givenValue := ""
	expected := "NULL"

	actual := nullType.Translate(givenValue)

	assert.Equals(expected, actual)
}
