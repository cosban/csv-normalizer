package main

import (
	"errors"
	"fmt"
	"io"
	"os"

	"gitlab.com/cosban/normalize/internal"
)

func getFile(filename string) io.Reader {
	fileinfo, err := os.Lstat(filename)
	if err != nil {
		fmt.Printf("Unable to use file: %s\n\n", filename)
		help()
	} else if fileinfo.IsDir() {
		fmt.Printf("Expected file, not directory\n\n")
		help()
	}
	file, err := os.Open(filename)
	if err != nil {
		fmt.Printf("Unable to read file: %s\n\n", filename)
		help()
	}
	return file
}

func validateArgs(args []string) error {
	if len(args) < 1 {
		return errors.New("Invalid number of arguments")
	}
	return nil
}

func help() {
	fmt.Print(internal.HelpText)
	os.Exit(0)
}

func version() {
	fmt.Print(internal.VersionText)
	os.Exit(0)
}
