package internal

type StringSet map[string]bool

func StringSetOf(v ...string) *StringSet {
	set := &StringSet{}
	set.Put(v...)
	return set
}

func (s *StringSet) Put(v ...string) {
	for _, val := range v {
		(*s)[val] = true
	}
}

func (s *StringSet) Remove(v string) {
	delete(*s, v)
}

func (s *StringSet) Contains(v string) bool {
	if b, ok := (*s)[v]; ok && b {
		return true
	}
	return false
}
