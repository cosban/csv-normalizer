package types

import (
	"testing"

	"github.com/cosban/assert"
)

var (
	decimalType = &DecimalType{}
)

func Test_dec_verify_happy(t *testing.T) {
	assert := assert.New(t)

	givenValue := "150.03333"

	assert.True(decimalType.Verify(givenValue))
}

func Test_dec_verify_string_sad(t *testing.T) {
	assert := assert.New(t)

	givenValue := "mr mittens"

	assert.False(decimalType.Verify(givenValue))
}

func Test_dec_verify_int_sad(t *testing.T) {
	assert := assert.New(t)

	givenValue := "42"

	assert.False(decimalType.Verify(givenValue))
}

func Test_dec_translate_dollar(t *testing.T) {
	assert := assert.New(t)

	givenValue := "$42.12"
	expected := "42.12"

	assert.Equals(expected, decimalType.Translate(givenValue))
}
