package internal

type DataType interface {
	Verify(string) bool
	Translate(string) string
	Type() string
}
