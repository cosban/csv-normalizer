package types

import "strconv"

type BooleanType struct{}

func (t *BooleanType) Verify(value string) bool {
	if value == "0" || value == "1" {
		return false
	}
	_, err := strconv.ParseBool(value)
	return err == nil || n.Verify(value)
}

func (t *BooleanType) Translate(value string) string {
	if n.Verify(value) {
		return n.Translate(value)
	}
	if b, _ := strconv.ParseBool(value); b {
		return "1::bit"
	}
	return "0::bit"
}

func (t *BooleanType) Type() string {
	return "bit"
}
